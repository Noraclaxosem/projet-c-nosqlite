#include "mynosql_lite.h"

t_hashmap* JSON_parse(char* string){
    t_hashmap t = hashmap_create(100,0,0);


    return t;
}

char* JSON_stringify(t_hashmap* map){
    return "";
}

void* hashmap_get(t_hashmap* map, char* pth){
    return NULL;
}

void hashmap_put(t_hashmap* map, char* path, void* value){
    map->entries->key = path;
    map->entries->value = value;
}

t_hashmap* hashmap_create(unsigned int slots, double lf, double gf){
    t_hashmap* m = (t_hashmap*) malloc(sizeof(t_hashmap));
	m->slots = slots;
	t->size = 0;
	m->size = 0;
    m->grow_factor = 2;
    m->load_factor = 0.7;
    m->entries = (t_hashmap_entry**)calloc(sizeof(t_hashmap_entry*));
	return m;
}

t_hashmap_entry* hashmap_entry_create(char* key, void* value){
    t_hashmap_entry *entry = (t_hashmap_entry*)malloc(sizeof(t_hashmap_entry));
    entry->value = value;
    entry->key = key;
    return entry;
}
