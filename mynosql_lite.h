#ifndef MYNOSQL_LITE_H_INCLUDED
#define MYNOSQL_LITE_H_INCLUDED

typedef struct s_hashmap_entry {
    void *value;
    char *key;
    struct s_hashmap_entry *next;
} t_hashmap_entry;

typedef struct s_hashmap{
	t_hashmap_entry **entries;
	unsigned int slots;
	double load_factor;
	double grow_factor;
	unsigned int size;
} t_hashmap;

t_hashmap* JSON_parse(char*);

char* JSON_stringify(t_hashmap*);

void* hashmap_traverse(t_hashmap*, char*);

void hashmap_put(t_hashmap*, char*, void*);

t_hashmap* hashmap_create(unsigned int, double, double);

t_hashmap_entry* hashmap_entry_create(char*, void*);

#endif // MYNOSQL_LITE_H_INCLUDED
